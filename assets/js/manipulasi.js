const dataMenu = [
    //index[0] atau item-1
    {
        gambarMakanan : "assets/img/menu-1.png",
        namaMakanan   : "<h3>PROMO MERDEKA</h3>",
    }, 
    //index[1] atau item-2
    {
        gambarMakanan : "assets/img/menu-2.jpg",
        namaMakanan   : "<h3>LIMITED TIME</h3>",
    },
    //index[2] atau item-3
    {
        gambarMakanan : "assets/img/menu-3.jpg",
        namaMakanan   : "<h3>KING OF THE WEEK</h3>",
    },
    //index[3] atau item-4
    {
        gambarMakanan : "assets/img/menu-4.png",
        namaMakanan   : "<h3>KING SAVERS</h3>",
    },
    //index[4] atau item-5
    {
        gambarMakanan : "assets/img/menu-5.jpg",
        namaMakanan   : "<h3>KING DEALS</h3>",
    },
    //index[5] atau item-6
    {
        gambarMakanan : "assets/img/menu-6.jpg",
        namaMakanan   : "<h3>BURGER BESAR</h3>",
    },
    //index[6] atau item-7
    {
        gambarMakanan : "assets/img/menu-7.jpg",
        namaMakanan   : "<h3>BURGER</h3>",
    },
    //index[7] atau item-8
    {
        gambarMakanan : "assets/img/menu-8.jpg",
        namaMakanan   : "<h3>FRIED CHICKEN</h3>",
    },
    //index[8] atau item-9
    {
        gambarMakanan : "assets/img/menu-9.jpg",
        namaMakanan   : "<h3>SNACKS</h3>",
    },
    //index[9] atau item-10
    {
        gambarMakanan : "assets/img/menu-10.jpg",
        namaMakanan   : "<h3>BEVERAGES</h3>",
    }
];

const body = document.querySelector(".menu-box");

for(let data of dataMenu){
    body.innerHTML +=  `<div class="card-menu">
                                <a href="#">
                                        <img src="${data.gambarMakanan}"/>
                                    <div class="nama-menu">
                                        <div class="two-column">
                                            <h3 class="title">${data.namaMakanan}</h3>
                                        </div>
                                        <div class="two-column">
                                            <button class="button">ORDER</button>
                                        </div>
                                    </div>
                                </a>
                            </div>`;
};

console.log("bisa ga")
